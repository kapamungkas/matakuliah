<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');

        $login = User::where('username',$username)->first();
        if(!$login){
            $res['success']=false;
            $res['result']='username or password incorrect';
            return response($res);
        }else{
            if (Hash::check($password,$login->password)) {
                $api_token = sha1(time());
                $create_token = User::where('id',$login->id)->update(['api_token'=>$api_token]);
                if ($create_token) {
                    $res['success']=true;
                    $res['api_token'] = $api_token;
                    $res['result']="Login Success";
                    return response($res);
                }
            }else{
                $res['success']=false;
                $res['result']='username or password incorrect 2';
                $res['password']= $password;
                return response($res);
            }
        }

    }

    public function cekToken(Request $request,$token){
      $login = User::where('api_token',$token)->first();
      if ($login) {
        $res['success']=true;
        $res['api_token'] = $token;
        $res['id_user']=$login->id;
        $res['id_role']=$login->id_role;
        $res['result']="Login Success";
        return response($res);
      }else{
        $res['success']=false;
        $res['result']='username or password incorrect 2';
        return response($res);
      }
    }



    //
}
