<?php


namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request)
    {

        $username = $request->input('username');
        $password = Hash::make($request->input('password'));
        $register = User::create([
            'username' => $username,
            'password' => $password,
            'role'=>'mahasiswa'
        ]);

        if ($register) {
            $res['success'] = true;
            $res['result'] = 'Register Success !!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['result'] = 'Register Failed !!';
            return response($res);
        }
    }
}
