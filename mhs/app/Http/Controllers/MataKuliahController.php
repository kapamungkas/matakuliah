<?php

namespace App\Http\Controllers;

use App\MataKuliah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MataKuliahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function getMataKuliah(Request $request)
    {
        $user = MataKuliah::select('kdmk','matkul')
            ->get();
        if ($user) {
            # code...
            $res['success'] = true;
            $res['message'] = "Succes get data";
            $res['datas'] = $user;
            return response($res);
        } else {
            $res['success'] = false;
            $res['result'] = 'canot find data';
            return response($res);
        }
        return response($res);
    }

    public function getMataKuliahSearch(Request $request)
    {
        $user = MataKuliah::select('kdmk','matkul')
            ->where('matkul', 'like', ''.$request->keyword.'%')
            ->get();
        if ($user) {
            # code...
            $res['success'] = true;
            $res['message'] = "Succes get data";
            $res['datas'] = $user;
            return response($res);
        } else {
            $res['success'] = false;
            $res['result'] = 'canot find data';
            return response($res);
        }
        return response($res);
    }

    

}
