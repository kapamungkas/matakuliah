<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class MataKuliah extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'mk';
    protected $fillable = [
      'kdmkg','kdmk','matkul','matkul_en','nmalias','teori','prak','jnsmk','jnsuji','smtr','status','noid'
    ];
    protected $primaryKey = 'kdmkg';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
}
