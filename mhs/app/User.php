<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = [
        'username','password','api_token','role'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */


    // public function authorizeRoles($roles)
    // {
    //     if(){

    //     }else{
    //         abort(401, 'This action is unauthorized.');
    //     }
    //   if (is_array($roles)) {
    //       return $this->hasAnyRole($roles) ||
    //              abort(401, 'This action is unauthorized.');
    //   }
    //   return $this->hasRole($roles) ||
    //          abort(401, 'This action is unauthorized.');
    // }
}
