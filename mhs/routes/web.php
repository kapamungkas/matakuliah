<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/login', 'LoginController@login');
$router->get('/cekToken/{token}', 'LoginController@cekToken');
$router->post('/register', 'RegisterController@register');

//
$router->get('/matakuliah', ['middleware'=>'auth','uses'=>'MataKuliahController@getMataKuliah']);
$router->get('/searchmatakuliah', ['middleware'=>'auth','uses'=>'MataKuliahController@getMataKuliahSearch']);
