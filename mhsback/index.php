<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<script src="dist/js/req.js"></script>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">LUMEN TESTING</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="logout">Logout</a>


</nav>

<main role="main" class="container">
    <input type="text" class="form-control carinama" placeholder="Nama Mata Kuliah">
    <table class="tabel table">
        <tr>
            <td>Kode</td>
            <td>Nama</td>
        </tr>
    </table>

</main><!-- /.container -->
<style>
    body {
        padding-top: 5rem;
    }

    .starter-template {
        padding: 3rem 1.5rem;
        text-align: center;
    }
    .logout{color: #fff;}
</style>
<script>
    if(sessionStorage.getItem("api_token") == null){
        window.location.href = 'login.php';
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'GET',
        url: ip + 'mhs/public/matakuliah',
        datatype: 'json',
        data: {
            'api_token': sessionStorage.getItem("api_token")
        },
        success: function(data) {
            $.each(data.datas, function(i, val) {

                $(".tabel").append("<tr><td>" + val['kdmk'] + "</td><td>" + val['matkul'] + "</td><tr>");

            });
        }
    })

    $('.carinama').keyup(function() {
        $.ajax({
            type: 'GET',
            url: ip + 'mhs/public/searchmatakuliah',
            datatype: 'json',
            data: {
                'api_token': sessionStorage.getItem("api_token"),
                'keyword': $(this).val()
            },
            success: function(data) {
                $(".tabel").html("<tr><td>Kode</td><td>Nama</td></tr>")
                $.each(data.datas, function(i, val) {

                    $(".tabel").append("<tr><td>"+val['kdmk']+"</td><td>"+val['matkul']+"</td><tr>");

                });
            }
        })
    })

    $('.logout').click(function(){
        sessionStorage.removeItem("api_token");
        location.reload();
    })
</script>