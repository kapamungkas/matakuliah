<?php include 'koneksi.php';?>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php 
if(!isset($_SESSION['login_user'])){
    header("location:login.php");
}
?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Absensi</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="?page=home">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Golongan</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="?page=golongan">List</a>
                    <a class="dropdown-item" href="?page=tambah_golongan">Tambah</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pegawai</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="?page=pegawai">List</a>
                    <a class="dropdown-item" href="?page=tambah_pegawai">Tambah</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shift</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="?page=shift">List</a>
                    <a class="dropdown-item" href="?page=tambah_shift">Tambah</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Laporan</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="?page=laporan">Absensi</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php">Logout</a>
            </li>
        </ul>
    </div>
</nav>

<main role="main" class="container">
    <?php
    if($_GET['page']=='home'){
        include 'template/home.php';
    }elseif($_GET['page']=='pegawai'){
        include 'template/pegawai.php';
    }elseif($_GET['page']=='tambah_pegawai'){
        include 'template/tambah_pegawai.php';
    }elseif($_GET['page']=='golongan'){
        include 'template/golongan.php';
    }elseif($_GET['page']=='tambah_golongan'){
        include 'template/tambah_golongan.php';
    }elseif($_GET['page']=='shift'){
        include 'template/shift.php';
    }elseif($_GET['page']=='tambah_shift'){
        include 'template/tambah_shift.php';
    }elseif($_GET['page']=='laporan'){
        include 'template/laporan.php';
    }elseif($_GET['page']=='ubah_golongan'){
        include 'template/ubah_golongan.php';
    }elseif($_GET['page']=='ubah_pegawai'){
        include 'template/ubah_pegawai.php';
    }elseif($_GET['page']=='ubah_shift'){
        include 'template/ubah_shift.php';
    }else{
        include 'template/home.php';
    }
    ?>
    

</main><!-- /.container -->
<style>
    body {
        padding-top: 5rem;
    }

    .starter-template {
        padding: 3rem 1.5rem;
        text-align: center;
    }
</style>
